package com.example.mycity.repository

import com.example.mycity.api.RetrofitInstance

class CityRepository {

    suspend fun getCity(countryIds: String) =
        RetrofitInstance.api.getCity(countryIds)
}
