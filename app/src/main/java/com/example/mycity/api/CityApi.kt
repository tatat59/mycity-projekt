package com.example.mycity.api

import com.example.mycity.model.CityHu
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface CityApi {


    @GET("/v1/geo/cities")
    suspend fun getCity(
        @Query("countryIds") countryIds: String,
    ): Response<CityHu>

}