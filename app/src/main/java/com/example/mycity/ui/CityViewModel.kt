package com.example.mycity.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mycity.model.CityHu
import com.example.mycity.repository.CityRepository
import com.example.mycity.util.Resource
import kotlinx.coroutines.launch
import retrofit2.Response

class CityViewModel() : ViewModel() {

    val TAG = "CityViewModelTAG"

    val repo = CityRepository()
    val cities: MutableLiveData<Resource<CityHu>> = MutableLiveData()

    init {
        //getCity("HU")
        getCity("US")
    }

    fun getCity(cityId: String) = viewModelScope.launch {
        cities.postValue(Resource.Loading())
        val response = repo.getCity(cityId)
        cities.postValue(handleCityResponse(response))
    }

    private fun handleCityResponse(response: Response<CityHu>): Resource<CityHu>? {
        if (response.isSuccessful) {
            response.body()?.let { resultResponse ->
                return Resource.Success(resultResponse)
            }
        }
        return Resource.Error(response.message())
    }
}