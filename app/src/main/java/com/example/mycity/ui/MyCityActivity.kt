package com.example.mycity.ui

import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.pm.PackageManager
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.mycity.R
import com.example.mycity.ui.fragments.CityFragment
import androidx.core.content.ContextCompat


class MyCityActivity : AppCompatActivity() {

    val viewModel: CityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mycity)

        getPermission()

        getFragment()
    }

    private fun getFragment() {

        val cityFragment: CityFragment
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()

        if (fm.findFragmentByTag(CityFragment.TAG) != null) {
            cityFragment = fm.findFragmentByTag(CityFragment.TAG) as CityFragment
            ft.replace(R.id.fragmentframelayout, cityFragment)
            ft.commit()
        } else {
            cityFragment = CityFragment.newInstance("p1", "p2")
            ft.add(R.id.fragmentframelayout, cityFragment, CityFragment.TAG)
            ft.commit()
            Toast.makeText(this, "CityFragment is coming ...", Toast.LENGTH_SHORT).show()
        }
    }

    private fun getPermission() {
        if (ContextCompat.checkSelfPermission(this,ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this, arrayOf(ACCESS_FINE_LOCATION), 1)
            } else {
                ActivityCompat.requestPermissions(this,arrayOf(ACCESS_FINE_LOCATION), 1)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray,
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] === PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    ) {
                        Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show()
                }
                return
            }
        }
    }

}