package com.example.mycity.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.mycity.R
import com.example.mycity.ui.CityViewModel
import com.example.mycity.ui.MyCityActivity
import com.example.mycity.util.Resource
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_city.*

class CityFragment : Fragment(R.layout.fragment_city), OnMapReadyCallback {


    lateinit var viewModel: CityViewModel
    val TAG = "CityFragmentTag"
    private lateinit var myMap: GoogleMap

    private var lat: Double = 0.0
    private var long: Double = 0.0

    private var myCityData: Pair<Double, Double> = Pair(0.0, 0.0)
    private var cityName: String = ""

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CityFragment().apply {
            }

        const val TAG = "CityFragmentTag"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = (activity as MyCityActivity).viewModel
    }


    fun getMyMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.citymenu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        cityName = item.title.toString()
        when (item.itemId) {
            //R.id.baja -> {
            R.id.abilene -> {
                Toast.makeText(context, cityName, Toast.LENGTH_SHORT).show()
                tvCityName.text = cityName
                myCityData = getCity(cityName)
                getMyMap()
            }
            //R.id.bkeszi -> {
            R.id.abington -> {
                Toast.makeText(context, cityName, Toast.LENGTH_SHORT).show()
                tvCityName.text = cityName
                myCityData = getCity(cityName)
                getMyMap()
            }
            //R.id.angyal -> {
            R.id.acadia -> {
                Toast.makeText(context, cityName, Toast.LENGTH_SHORT).show()
                tvCityName.text = cityName
                myCityData = getCity(cityName)
                getMyMap()
            }
            //R.id.beretty -> {
            R.id.ada -> {
                Toast.makeText(context, cityName, Toast.LENGTH_SHORT).show()
                tvCityName.text = cityName
                myCityData = getCity(cityName)
                getMyMap()
            }
            R.id.adams -> {
                Toast.makeText(context, cityName, Toast.LENGTH_SHORT).show()
                tvCityName.text = cityName
                myCityData = getCity(cityName)
                getMyMap()
            }
        }
        return super.onOptionsItemSelected(item)
    }


    fun getCity(cityName: String): Pair<Double, Double> {

        var myCityLat = 0.0
        var myCityLong = 0.0


        viewModel.cities.observe(viewLifecycleOwner, Observer { response ->
            when (response) {
                is Resource.Success -> {
                    response.data.let { cityResponse ->
                        cityResponse?.data?.forEach {
                            if (it.name.contains(cityName)) {
                                myCityLat = it.latitude
                                myCityLong = it.longitude
                            }
                        }
                    }
                }
                is Resource.Error -> {
                    response.message?.let { message ->
                        Log.e(TAG, "Network Error : $message")
                    }
                }
            }
        })
        return Pair(myCityLat, myCityLong)
    }


    override fun onMapReady(googleMap: GoogleMap) {

        myMap = googleMap
        myMap.clear()
        lat = myCityData.first
        long = myCityData.second
        //val hungary = LatLng(lat, long)
        //val cameraUpdate = CameraUpdateFactory.newLatLngZoom(hungary, 10F)
        val unitedStates = LatLng(lat, long)
        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(unitedStates, 10F)
        myMap.uiSettings.isZoomControlsEnabled = true
        //myMap.addMarker(MarkerOptions().position(hungary).title("Marker in Hungary"))
        myMap.addMarker(MarkerOptions().position(unitedStates).title("Marker in United States"))
        myMap.moveCamera(cameraUpdate)
    }

}