package com.example.mycity.model

data class CityHu(
    val `data`: List<Data>,
    val links: List<Link>,
    val metadata: Metadata
) {
    data class Data(
        val city: String,
        val country: String,
        val countryCode: String,
        val id: Int,
        val latitude: Double,
        val longitude: Double,
        val name: String,
        val population: Int,
        val region: String,
        val regionCode: String,
        val regionWdId: String,
        val type: String,
        val wikiDataId: String
    )

    data class Link(
        val href: String,
        val rel: String
    )

    data class Metadata(
        val currentOffset: Int,
        val totalCount: Int
    )
}