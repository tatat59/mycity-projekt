package com.example.mycity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.mycity.model.User
import com.example.mycity.ui.MyCityActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val mUser = User("User", "Password")

        btnLogin.setOnClickListener {
            if (etName.text.toString() == mUser.mName && etPassword.text.toString() == mUser.mPassword) {
                val intentMyCityActivity = Intent(this, MyCityActivity::class.java)
                startActivity(intentMyCityActivity)
            } else {
                Toast.makeText(this, "Hibás felhasználói adatok", Toast.LENGTH_SHORT).show()
            }
        }
    }


}